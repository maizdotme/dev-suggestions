import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

Vue.use(ElementUI);

//路由守卫，在跳转前检查是否登录，如果没有登录则跳转到登录
//详见 https://router.vuejs.org/zh/guide/advanced/navigation-guards.html
router.beforeEach((to, from, next) => {
	
	let isAuthenticated = localStorage.getItem('isLogin')=='true';

	console.log(to.path !== '/login' && !isAuthenticated)
	if (to.path !== '/login' && !isAuthenticated) {
		alert('您还没有登录，请先登录');
		console.log('未登录，跳转登录页')
		next({
			path:'/login'
		})
	} else {
		next()
	}
});


new Vue({
	router,
	render: h => h(App)
}).$mount('#app')
