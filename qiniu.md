七牛提供对象存储，支持cdn加速，标准存储每月10G免费空间，域名访问每月更换一次

官方Java SDK 文档 https://developer.qiniu.com/kodo/sdk/1239/java

操作步骤：

1. 注册七牛云，用我的链接（https://portal.qiniu.com/signup?code=1h5npa9r5az4i）

2. 注册后提醒你要实名认证，照着做呗

3. 添加对象存储空间,基本默认,注意下以下几点

   - 空间名称就是后面上传要用到的`bucket`

   - 访问控制设为`“公开”`，后续图片需要访问所以必须公开

   - 对象存储>空间管理>XXX空间   右下角的`CDN测试域名` 是你的外网访问域名，文件上传后，直接跟在这个域名后就可以访问了。

     ![](http://q8ipsgm1k.bkt.clouddn.com/qiniu.png)

     比如我上传的图片：http://q8ipsgm1k.bkt.clouddn.com/2020-04-09T19:42:54.124_5015984-7fd73af768f28704.png 

4. access key 和 secret key 在右上角用户头像 下拉 的`密钥管理` 里 

5. 上传操作

   - 引入SDK，maven依赖为

     ```xml
     <dependency>
         <groupId>com.qiniu</groupId>
         <artifactId>qiniu-java-sdk</artifactId>
         <version>[7.2.0, 7.2.99]</version>
     </dependency>
     <!--gson用于七牛云的结果反序列化-->
     <dependency>
         <groupId>com.google.code.gson</groupId>
         <artifactId>gson</artifactId>
         <version>2.8.6</version>
     </dependency>
     
     ```
     
    - 编写上传Util
   
      ```java
      package me.maiz.trainning.devsuggestions.qiniuupload;
      
      import com.google.gson.Gson;
      import com.qiniu.common.QiniuException;
      import com.qiniu.http.Response;
      import com.qiniu.storage.Configuration;
      import com.qiniu.storage.Region;
      import com.qiniu.storage.UploadManager;
      import com.qiniu.storage.model.DefaultPutRet;
      import com.qiniu.util.Auth;
      
      import java.io.InputStream;
      
      public class QiniuUtil {
      
          //设置识别用的各种KEY
          private static final String ACCESS_KEY = "你自己的ak";
          private static final String SECRET_KEY = "你自己的sk";
          private static final String BUCKET = "你自己的空间名称";
      
          public static void upload(String fileKey, InputStream fileStream) {
              //提供配置信息
              Configuration cfg = new Configuration(Region.region0());
              UploadManager uploadManager = new UploadManager(cfg);
              Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
              String upToken = auth.uploadToken(BUCKET);
      
              Response response = null;
              try {
                  //上传
                  response = uploadManager.put(fileStream, fileKey, upToken,null,null);
                  //解析上传成功的结果
                  DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
                  System.out.println(putRet.key);
                  System.out.println(putRet.hash);
              } catch (QiniuException e) {
                throw new RuntimeException("上传文件出错",e);
              }
          }
      
          private static final String CDN_DOMAIN = "你自己的CDN测试域名";
          
          //生成用于全局访问的网络地址，实际应用时可以直接存入数据库
          public static String fullPath(String fileKey){
              return CDN_DOMAIN+fileKey;
          }
      
      }
      
      ```
   
      
   

- 