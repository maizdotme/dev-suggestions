package me.maiz.trainning.devsuggestions;

import lombok.Data;

@Data
public class UserForm {
    private String username;
    private int age;
    private String birthday;
}
