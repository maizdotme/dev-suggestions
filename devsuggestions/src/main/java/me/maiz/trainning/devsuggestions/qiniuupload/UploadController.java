package me.maiz.trainning.devsuggestions.qiniuupload;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
@Slf4j
public class UploadController {

    @PostMapping("upload")
    @ResponseBody
    public String upload(MultipartFile avatar){
        log.info("参数："+avatar);
        String fileName = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)+"_"+avatar.getOriginalFilename();
        try {
            log.info("文件名："+fileName);
            QiniuUtil.upload(fileName,avatar.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return QiniuUtil.fullPath(fileName);
    }


}
