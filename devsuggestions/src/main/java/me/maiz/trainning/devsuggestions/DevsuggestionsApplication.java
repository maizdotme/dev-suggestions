package me.maiz.trainning.devsuggestions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevsuggestionsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevsuggestionsApplication.class, args);
    }

}
