# maven私服使用方式

在学校内网架设了一个私服，便于大家共享jar包。

Web管理地址： http://192.168.120.111:8081/ 

以下是配置方式

### 一、修改maven配置

位置：maven安装目录/conf/settings.xml
在配置文件中找到以下内容并添加

```xml
...
  <servers>
    <!-- ①管理员账户配置 -->
    <server>
      <id>nexus</id>
      <username>admin</username>
      <password>admin123</password>
    </server>
  </servers>
...
<mirrors>
    <!--②私服镜像配置-->
    <mirror>
        <id>nexus</id>
        <mirrorOf>*</mirrorOf>
        <url>http://192.168.120.111:8081/repository/maven-public/</url>
    </mirror>
    <mirror>
        <id>aliyunmaven</id>
        <mirrorOf>*</mirrorOf>
        <name>阿里云公共仓库</name>
        <url>https://maven.aliyun.com/repository/public</url>
    </mirror> 
</mirrors>
...
<profile>
  <!--③nexus的profile配置-->
      <id>nexus</id>
      <repositories>
        <repository>
          <id>central</id>
          <url>http://central</url>
          <releases><enabled>true</enabled></releases>
          <snapshots><enabled>true</enabled></snapshots>
        </repository>
      </repositories>
     <pluginRepositories>
        <pluginRepository>
          <id>central</id>
          <url>http://central</url>
          <releases><enabled>true</enabled></releases>
          <snapshots><enabled>true</enabled></snapshots>
        </pluginRepository>
      </pluginRepositories>
    </profile>

  </profiles>
  <!-- ④激活以上profile -->
  <activeProfiles>
    <activeProfile>nexus</activeProfile>
  </activeProfiles>

```



### 二、项目配置

在项目的**“父POM”** 中加入以下配置：

```xml
...


<!-- 用于发布项目 -->
    <distributionManagement>
      <repository>
          <id>nexus</id>
          <name>Releases</name>
          <url>http://192.168.120.111:8081/repository/maven-releases</url>
        </repository>
        <snapshotRepository>
          <id>nexus</id>
          <name>Snapshot</name>
          <url>http://192.168.120.111:8081/repository/maven-snapshots</url>
        </snapshotRepository>
    </distributionManagement>

<!-- 用于拉取项目 -->
    <repositories>
        <repository>
            <id>maven-nexus</id>
            <name>maven-nexus</name>
            <url>http://192.168.120.111:8081/repository/maven-public/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
...
```



### 三、发布及拉取

发布： 在项目根目录下执行 `mvn clean deploy -Dmaven.test.skip=true` 其中maven.test.skip=true 参数时为了跳过单元测试。



可以在  http://192.168.120.111:8081/#browse/browse:maven-snapshots  查看是否发布成功

拉取：在需要的项目中添加相应的依赖，重新编译即可拉取

```xml
<dependency>
  <groupId>com.woniuxy.cloud</groupId>
  <artifactId>user-facade</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```



### 四、多模块项目如何处理

如果你的项目架构中存在一个多个项目的公共父POM（例如，ticket-parent），则考虑在其中添加 二中的提到的distributionManagement

分布式项目之间存在一个接口共享的问题，需要用到maven私服，此时应该只部署facade接口包。请在其他子模块pom的\<properties>标签中加入,避免出现发布核心代码的问题。

```xml
<maven.deploy.skip>true</maven.deploy.skip>
```

