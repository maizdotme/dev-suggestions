# 云效使用说明

云效是阿里提供的devops平台。提供了代码管理、bug管理、持续集成流水线等功能
https://www.aliyun.com/product/yunxiao

我们使用的是云效的制品仓库 https://packages.aliyun.com/maven 即maven私服。主要用于在公司内部各团队共享jar包。

使用方式：

1. 注册开通  https://packages.aliyun.com/maven

   ![image-20210923142828176](https://woniumd.oss-cn-hangzhou.aliyuncs.com/java/lvchen/20210923142828.png)

2. 初次打开时，点击仓库，打开仓库指南，则可按照指示配置，直接复制代码即可，密码等已经在指南代码中设置好了，第三步系搬运至此文档，以仓库指南为准

![image-20210923143137113](https://woniumd.oss-cn-hangzhou.aliyuncs.com/java/lvchen/20210923143137.png)

3. 配置本地maven的settings.xml，请确保你修改的是idea中使用的配置文件

- 设置仓库凭证

  ```xml
  <servers>
    <server>
      <id>rdc-releases</id>
      <username>60ed6298aa6381038e0a29ad</username>
      <password>im1MXPf-(gws</password>
    </server>
    <server>
      <id>rdc-snapshots</id>
      <username>60ed6298aa6381038e0a29ad</username>
      <password>im1MXPf-(gws</password>
    </server>
  </servers>
  ```

- 制品上传配置

  ```xml
  <profiles>
    <profile>
      <id>rdc</id>
      <properties>
          <!-- 正式发布仓库 -->
        <altReleaseDeploymentRepository>
          rdc-releases::default::https://packages.aliyun.com/maven/repository/2140026-release-j0Re0H/
        </altReleaseDeploymentRepository>
          <!-- 快照仓库 -->
        <altSnapshotDeploymentRepository>
          rdc-snapshots::default::https://packages.aliyun.com/maven/repository/2140026-snapshot-EjwWls/
        </altSnapshotDeploymentRepository>
      </properties>
    </profile>
  </profiles>
  
  <activeProfiles>
    <activeProfile>rdc</activeProfile>
  </activeProfiles>
  ```

- mirrors配置以加速下载

```xml
<mirrors>
  <mirror>
    <id>mirror</id>
    <mirrorOf>central,jcenter,!rdc-releases,!rdc-snapshots</mirrorOf>
    <name>mirror</name>
    <url>https://maven.aliyun.com/nexus/content/groups/public</url>
  </mirror>
</mirrors>
```

- 在需要推送的maven项目下执行推送命令 ` mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests`  即可推送到私服仓库

- 配置下载的仓库地址，放在前面配置的\<profile>标签下

  ![image-20210923150250857](https://woniumd.oss-cn-hangzhou.aliyuncs.com/java/lvchen/20210923150251.png)

```xml
<repositories>
  <repository>
    <id>rdc-releases</id>
    <url>https://packages.aliyun.com/maven/repository/2140026-release-j0Re0H/</url>
    <releases>
      <enabled>true</enabled>
    </releases>
    <snapshots>
      <enabled>false</enabled>
    </snapshots>
  </repository>
  <repository>
    <id>rdc-snapshots</id>
    <url>https://packages.aliyun.com/maven/repository/2140026-snapshot-EjwWls/</url>
    <releases>
      <enabled>false</enabled>
      </releases>
    <snapshots>
      <enabled>true</enabled>
    </snapshots>
  </repository>
</repositories>
```

- 在需要用到依赖的pom.xml中添加依赖，执行 mvn  install命令即可下载仓库

PS: 在idea中执行依赖拉取操作，建议使用maven命令方式而不是直接点重新导入
