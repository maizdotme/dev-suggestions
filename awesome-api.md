# 可用的API列表

### 敏感词过滤

| 名称  | 描述       | Auth | Https | 地址                                                   |
| ----- | ---------- | ---- | ----- | ------------------------------------------------------ |
| Hoapi | 敏感词过滤 | No   | NO    | [查看](http://www.hoapi.com/index.php/Home/Index/demo) |

### 快递

| 名称    | 描述         | Auth   | HTTPS | 地址                                                     |
| ------- | ------------ | ------ | ----- | -------------------------------------------------------- |
| 快递100 | 快递查询接口 | apiKey | Yes   | [查看](https://www.kuaidi100.com/openapi/applyapi.shtml) |
| 快递鸟  | 快递查询     | apikey | Yes   | [查看](http://www.kdniao.com/api-track)                  |

 https://github.com/TonnyL/Awesome_APIs/blob/master/README-zh.md 

 https://www.zhihu.com/question/32225726 

 https://github.com/yuyang2016/Chinese-Free-API 

 https://github.com/public-apis/public-apis 