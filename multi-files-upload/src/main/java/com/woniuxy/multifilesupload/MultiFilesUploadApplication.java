package com.woniuxy.multifilesupload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiFilesUploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiFilesUploadApplication.class, args);
    }

}
