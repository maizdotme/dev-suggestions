package com.woniuxy.multifilesupload.controller.form;

import lombok.Data;

import java.util.List;
@Data
public class FileItem {
    private String name;

    private String url;
}

