package com.woniuxy.multifilesupload.controller.form;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class FileUploadForm {

    private MultipartFile file;

    private String type;



}
