package com.woniuxy.sessiontest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@Slf4j
public class TestController {

    @GetMapping("testSession1")
    public Result set(int id, HttpSession session){
        log.info("接收到请求，id={}",id);
        session.setAttribute("id",id);

        return Result.success();
    }

    @GetMapping("testSession2")
    public Result get( HttpSession session){
        log.info("获取session值");

        Object val = session.getAttribute("id");

        return val==null?Result.fail("获取session失败"):Result.success(val);
    }


}
